﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Shirt : MonoBehaviour
{
    public static Shirt instance;
    public GameObject definedButton;
    public UnityEvent OnClick = new UnityEvent();
    public bool pressed = false;

    // Use this for initialization

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        definedButton = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit Hit;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == gameObject)
            {
                Debug.Log("Button Clicked");
                pressed = true;
                OnClick.Invoke();
            }
        }
    }
}
