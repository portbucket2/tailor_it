﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scale : MonoBehaviour
{
    public static Scale instance;
    public Transform transformObj;

    public float scaling;
    GameObject varObj;

    public Slider scale;
    // Start is called before the first frame update

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        scale.onValueChanged.AddListener(OnScaleSliderValueChanged);
        varObj = GameObject.Find("RayCastPosition");
        scaling = 0.8f;
    }

    

    // Update is called once per frame
    void Update()
    {
        ScaleUpOrDown(scaling);
    }

    private void OnScaleSliderValueChanged(float newValue)
    {
        scaling = newValue;
    }

    public void SetSelectedObject(Transform transformObj)
    {
        this.transformObj = transformObj;
    }

    public void ScaleUpOrDown(float scaleFactor)
    {
        if (transformObj != null && RotateAndScale.Instance.turnOn == false)
        {
            scale.interactable = true;
            Vector3 startScale = transformObj.localScale;
            startScale.x = scaleFactor;
            startScale.y = startScale.x;
            startScale.z = startScale.x;
            transformObj.localScale = startScale;
            //varObj.GetComponent<RayCastHitPosition>().enabled = false;
        }
       
        else
        {
            scale.interactable = false;
            //varObj.GetComponent<RayCastHitPosition>().enabled = true;
        }
        
    }
}
