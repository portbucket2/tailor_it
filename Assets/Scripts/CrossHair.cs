﻿using UnityEngine;

public class CrossHair : MonoBehaviour
{
    public int layerIdToCollide = 8;
    public int layerIdInside = 9;

    private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.gameObject.layer == layerIdToCollide)
        {
            collision.gameObject.SetActive(false);
            GameManager.instance.CheckLastPointCut();
        }
        else if (collision.gameObject.layer == layerIdInside)
        {
            
            //GameManager.instance.InsideCuttingDetected();
        }
    }
}
