﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public FinalTearableCloth finalTearableCloth;
    public DiceCollection diceCollection;
    public Material material;
    public Color fabricColor;
    public float mouseTearSize = 0.25f;

    public Transform crossHair, crossIcon;
    public Toggle fabricView;
    public int accuracy = 1;

    public float friction = 0.9999f;
    [Range(0, 1)]
    public float clothHardness = 1;
    public float tearSensitivity = 200;
    public float triangleCutout = 3f;
    public float restingDistances = 1;

    [Range(0, 1)]
    public float cuttingSpeed = 0.1f;

    public GameObject gameOverpanel;

    Vector3 mouseCurrentPosition, mousePreviousPosition;

    public List<Link> allLinesToRender = new List<Link>();

    public int totalRemainingPointsToCut;

    Vector3 offset = new Vector3 {

        x = 0,
        y = 200,
        z = 0f
    };
    Vector3 previousPos;

    bool gameState = false;

    private void Awake() {

        instance = this;
        DOTween.Init();
        crossHair.gameObject.SetActive(false);
        crossIcon.gameObject.SetActive(false);
        gameOverpanel.SetActive(false);
        bucket.transform.position = new Vector3(-137.5f, -135f, 0);
        diceMaterial.SetColor("_BaseColor", Color.white);
        //PlayerPrefs.GetInt("CL", 0);
        //PlayerPrefs.DeleteKey("CL");
    }

    private void Start()
    {

        StartCoroutine(finalTearableCloth.Setup());
        gameState = true;
    }

    bool isDragging;
    private void Update() {

        if (Input.GetMouseButtonDown(1))
            GameOver();

        if (gameState == false)
        {
            crossHair.gameObject.SetActive(false);
            crossIcon.gameObject.SetActive(false);
            isDragging = false;
            return;
        }

        if (Input.GetMouseButtonDown(0)) {

            crossHair.gameObject.SetActive(true);
            crossIcon.gameObject.SetActive(true);
            isDragging = true;

            Vector3 pos = GetWorldPositionOnPlane(Input.mousePosition + offset, 0);
            pos.z = -1f;
            crossIcon.position = pos;

            mouseCurrentPosition.x = pos.x;
            mouseCurrentPosition.y = pos.y;
            crossHair.position = PlaceCrossHairOutsieOfCloth();
                
        } 
        else if (Input.GetMouseButtonUp(0)) {

            crossHair.gameObject.SetActive(false);
            crossIcon.gameObject.SetActive(false);
            isDragging = false;
        }

        if (isDragging) {

            Vector3 pos = GetWorldPositionOnPlane(Input.mousePosition + offset, 0);
            pos.z = -1f;
            crossIcon.position = pos;

            pos = Vector3.Lerp(crossHair.position, pos, cuttingSpeed);
            mouseCurrentPosition.x = pos.x;
            mouseCurrentPosition.y = pos.y;
            crossHair.position = pos;

            Vector3 diff = crossHair.position - previousPos;
            float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

            if (angle != 0)
                crossHair.eulerAngles = new Vector3(0, 0, angle - 90);

        }
    }

    private Vector3 PlaceCrossHairOutsieOfCloth()
    {
        Vector3 screenPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        Vector3 returnVlaue = GetWorldPositionOnPlane(Input.mousePosition + offset, 0);
        returnVlaue.z = 0;

        float _offset = 2;
        if (screenPosition.x > screenPosition.y)
        {

            if (screenPosition.x < 0.5f)
                returnVlaue.x = -_offset;
            else
            {

                returnVlaue.x = finalTearableCloth.xSize + _offset;
            }

        }
        else
        {
            if (screenPosition.y < 0.5f)
                returnVlaue.y = _offset;
            else
                returnVlaue.y = -finalTearableCloth.ySize + _offset;

        }
        previousPos = returnVlaue;
        return returnVlaue;
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z) {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    private void LateUpdate() {

        mousePreviousPosition.x = mouseCurrentPosition.x;
        mousePreviousPosition.y = mouseCurrentPosition.y;
        previousPos = crossHair.position;
    }

    public void RestartGame()
    {
        //crossHair.gameObject.SetActive(false);
        //gameOverpanel.SetActive(false);
        //StartCoroutine(FinalTearableCloth.instance.Setup());
        Application.LoadLevel(0);
    }

    public bool DistPointToSegmentSquared(float pointX, float pointY) {

        return Vector2.Distance(mouseCurrentPosition, new Vector2(pointX, pointY)) < GameManager.instance.mouseTearSize;
    }

    public void CheckLastPointCut()
    {

        --totalRemainingPointsToCut;

        if (totalRemainingPointsToCut <= 0)
            GameOver();
    }

    public void InsideCuttingDetected()
    {

        GameOver();
    }

    public GameObject clothMesh;
    public GameObject bucket;
    public SpriteRenderer diceRenderer;
    public GameObject colorPanel;
    public Material diceMaterial;
    public Material bucketMaterial;
    public void GameOver()
    {

        //gameOverpanel.SetActive(true);
        //gameOverpanel.transform.GetChild(0).gameObject.SetActive(value);
        //gameOverpanel.transform.GetChild(1).gameObject.SetActive(!value);

        gameState = false;
        clothMesh.SetActive(true);
        diceRenderer.enabled = false;
        finalTearableCloth.transform.DOScale(Vector3.zero, 2f);
        finalTearableCloth.transform.DOMove(new Vector3(40f, -200.5f, 37.5f), 2f).OnComplete(()=> {
                       
            bucket.transform.DOMove(new Vector3(37.5f, -135f, 0), 2f).OnComplete(() => {

                colorPanel.SetActive(true);
            });
        });


    }

    public void ColorSelected(Image image)
    {
        
        colorPanel.SetActive(false);
        Color color = image.color;
        bucketMaterial.SetColor("_BaseColor", color);
        Vector3 oldPos = clothMesh.transform.position;
        clothMesh.transform.DOMove(bucket.transform.position, 2f).OnComplete(() => {

            diceMaterial.SetColor("_BaseColor", color);
            clothMesh.transform.DOMove(oldPos, 1f).OnComplete(() => {
                
                colorPanel.SetActive(true);
            });

        });
    }

    public void GoToPritingProcess()
    {

        Application.LoadLevel(1);
    }

    void OnEnable() {
        RenderPipelineManager.endCameraRendering += RenderPipelineManager_endCameraRendering;
    }
    void OnDisable() {
        RenderPipelineManager.endCameraRendering -= RenderPipelineManager_endCameraRendering;
    }
    private void RenderPipelineManager_endCameraRendering(ScriptableRenderContext context, Camera camera) {
        OnPostRender();
    }

    void OnPostRender() {

        if (!instance.material || !fabricView.isOn) {
            return;
        }
        GL.PushMatrix();
        instance.material.SetPass(0);
        GL.LoadPixelMatrix();

        GL.Begin(GL.LINES);
        GL.Color(fabricColor);
        for (int i = 0; i < allLinesToRender.Count; i++) {

            GL.Vertex(new Vector3(allLinesToRender[i].p1.x, allLinesToRender[i].p1.y, allLinesToRender[i].p1.z));
            GL.Vertex(new Vector3(allLinesToRender[i].p2.x, allLinesToRender[i].p2.y, allLinesToRender[i].p2.z));
        }

        GL.End();
        GL.PopMatrix();
    }
}
