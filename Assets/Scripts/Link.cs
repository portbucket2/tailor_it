﻿using UnityEngine;

public class Link {

    public PointMass p1;
    public PointMass p2;

    public Link(PointMass which1, PointMass which2, float restingDist, float stiff, float tearSensitivity) {

        p1 = which1; // when you set one object to another, it's pretty much a reference. 
        p2 = which2; // Anything that'll happen to p1 or p2 in here will happen to the paticles in our ArrayList

        GameManager.instance.allLinesToRender.Add(this);
    }

    public void OnClearLinks() {

        GameManager.instance.allLinesToRender.Remove(this);
    }

    // Solve the link constraint
    public void Solve() {

        // calculate the distance between the two PointMasss
        float diffX = p1.x - p2.x;
        float diffY = p1.y - p2.y;
        float diffZ = p1.z - p2.z;
        float d = Mathf.Sqrt(diffX * diffX + diffY * diffY + diffZ * diffZ);

        // find the difference, or the ratio of how far along the restingDistance the actual distance is.
        float difference = ( GameManager.instance.restingDistances - d) / d;

        // if the distance is more than curtainTearSensitivity, the cloth tears
        if (d > GameManager.instance.tearSensitivity)
            p1.RemoveLink(this);

        // Inverse the mass quantities
        float im1 = 1 / p1.mass;
        float im2 = 1 / p2.mass;
        float scalarP1 = (im1 / (im1 + im2)) * GameManager.instance.clothHardness;
        float scalarP2 = GameManager.instance.clothHardness - scalarP1;

        // Push/pull based on mass
        // heavier objects will be pushed/pulled less than attached light objects
        p1.x += diffX * scalarP1 * difference;
        p1.y += diffY * scalarP1 * difference;
        p1.z += diffZ * scalarP1 * difference;

        p2.x -= diffX * scalarP2 * difference;
        p2.y -= diffY * scalarP2 * difference;
        p2.z -= diffZ * scalarP2 * difference;
    }

}
