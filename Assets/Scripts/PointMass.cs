﻿using System.Collections.Generic;
using UnityEngine;

public class PointMass {

    float lastX, lastY, lastZ; // for calculating position change (velocity)
    public float x, y, z;
    float accX, accY, accZ;

    public float mass = 1f;

    // An ArrayList for links, so we can have as many links as we want to this PointMass
    public List<Link> links = new List<Link>();
    int triangleStartIndex;
    bool hasTriangle;

    bool pinned = false;
    float pinX, pinY, pinZ;

    // PointMass constructor
    public PointMass(Vector3 position, int triangleStartIndex, bool hasTriangle) {

        x = position.x;
        y = position.y;
        z = position.z;
        this.triangleStartIndex = triangleStartIndex;
        this.hasTriangle = hasTriangle;

        lastX = x;
        lastY = y;
        lastZ = z;

        accX = 0;
        accY = 0;
        accZ = 0;
    }
    
    // The update function is used to update the physics of the PointMass.
    // motion is applied, and links are drawn here
    public void UpdatePhysics(float timeStep) { // timeStep should be in elapsed seconds (deltaTime)
        
        ApplyForce(0, FinalTearableCloth.instance.gravity, 0);

        float velX = x - lastX;
        float velY = y - lastY;
        float velZ = z - lastZ;

        // dampen velocity
        velX *= GameManager.instance.friction;
        velY *= GameManager.instance.friction;
        velZ *= GameManager.instance.friction;

        float timeStepSq = timeStep * timeStep;
        // calculate the next position using Verlet Integration
        float nextX = x + velX + accX * timeStepSq;
        float nextY = y + velY + accY * timeStepSq;
        float nextZ = z + velZ + accZ * timeStepSq;

        // reset variables
        lastX = x;
        lastY = y;
        lastZ = z;

        x = nextX;
        y = nextY;
        z = nextZ;

        accX = 0;
        accY = 0;
        accZ = 0;
        
        SolveConstraints();
    }
    public void UpdateInteractions() {

        // this is where our interaction comes in.
        if (Input.GetMouseButton(0)) {

            if (GameManager.instance.DistPointToSegmentSquared(x, y))
                CutThisPoint();
        }
    }

    public void CutThisPoint() {

        for (int i = 0; i < links.Count; i++) {

            links[i].OnClearLinks();
        }

        links.Clear();

        if (hasTriangle) {

            hasTriangle = false;
            FinalTearableCloth.instance.UpdateTriangles(triangleStartIndex);
        }
    }


    public void draw() {

        // draw the links and points
        //stroke(0);
        //if (links.size() > 0) {
        //    for (int i = 0; i < links.size(); i++) {
        //        Link currentLink = (Link)links.get(i);
        //        currentLink.draw();
        //    }
        //} else
        //    point(x, y);
    }

    /* Constraints */
    public void SolveConstraints() {

        for (int j = 0; j < GameManager.instance.accuracy; j++)
        {
            /* Link Constraints */
            // Links make sure PointMasss connected to this one is at a set distance away
            for (int i = 0; i < links.Count; i++)
            {

                links[i].Solve();
            }

            /* Other Constraints */
            // make sure the PointMass stays in its place if it's pinned
            if (pinned)
            {
                x = pinX;
                y = pinY;
                z = pinZ;
            }

        }

        if (hasTriangle && FinalTearableCloth.instance.triangles.Length > triangleStartIndex + 6) {

            if (
                Vector3.Distance(
                    FinalTearableCloth.instance.vertices[FinalTearableCloth.instance.triangles[triangleStartIndex]],
                    FinalTearableCloth.instance.vertices[FinalTearableCloth.instance.triangles[triangleStartIndex + 2]]) > GameManager.instance.triangleCutout) {

                hasTriangle = false;
                FinalTearableCloth.instance.UpdateTriangles(triangleStartIndex);
            }
        }
    }

    // attachTo can be used to create links between this PointMass and other PointMasss
    public void attachTo(PointMass P, float restingDist, float stiff, float tearSensitivity) {

        Link lnk = new Link(this, P, restingDist, stiff, tearSensitivity);
        links.Add(lnk);
    }
    public void RemoveLink(Link lnk) {

        lnk.OnClearLinks();
        links.Remove(lnk);
    }

    public void ApplyForce(float fX, float fY, float fz) {

        // acceleration = (1/mass) * force
        // or
        // acceleration = force / mass
        accX += fX / mass;
        accY += fY / mass;
        accZ += fz / mass;
    }

    public void PinHook(float pX, float pY, float pZ) {

        pinned = true;
        pinX = pX;
        pinY = pY;
        pinZ = pZ;
    }

    public void PinTo() {

        pinned = true;
        pinX = x;
        pinY = y;
        pinZ = z;
        //FinalTearableCloth.instance.Set(new Vector3(x,y,z));
    }
}
