﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColorPallet : MonoBehaviour, IDragHandler
{
    public RawImage image;
    public Color color;

    public float d, m;

    Texture2D screenShoot;
    public void Awake()
    {
        screenShoot = image.texture as Texture2D;
    }

    public void OnDrag(PointerEventData data)
    {
        Vector3 v = Camera.main.WorldToScreenPoint( Input.mousePosition * m /d);
        color = screenShoot.GetPixel((int)v.x, (int)v.y);

        //var tex = new Texture2D(Screen.width, Screen.height);
        //tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        //tex.Apply();
        //color = tex.GetPixel((int)data.position.x, (int)data.position.y);
    }


    
}
