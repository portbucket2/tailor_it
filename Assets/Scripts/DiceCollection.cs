﻿using UnityEngine;

[CreateAssetMenu(menuName = "DiceCollection", fileName = "Dice")]
public class DiceCollection : ScriptableObject
{
    public Sprite[] sprites;
}
