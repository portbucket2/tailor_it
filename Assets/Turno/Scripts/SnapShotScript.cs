﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapShotScript : MonoBehaviour
{
    private static SnapShotScript instance;
    private Camera myCamera;
    private bool takeSSOnNextFrame;
    public Texture2D tshirtTexture;

    private void Awake()
    {
        instance = this;
        myCamera = gameObject.GetComponent<Camera>();

    }

    private void OnPostRender()
    {
        if(takeSSOnNextFrame)
        {
            takeSSOnNextFrame = false;
            myCamera.Render();
            RenderTexture renderTexture = myCamera.targetTexture;


            tshirtTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            tshirtTexture.ReadPixels(rect, 0, 0);

            //byte[] byteArray = renderResult.EncodeToPNG();
            //tshirtTexture.LoadRawTextureData(byteArray);
            //tshirtTexture.Apply();
            //System.IO.File.WriteAllBytes(Application.dataPath + "/CameraScreenshot.png", byteArray);
            Debug.Log("Saved CameraScreenshot.png");
            Tshirt.instance.ApplyTexture();
            //RenderTexture.ReleaseTemporary(renderTexture);
            //myCamera.targetTexture = null;

        }
    }

    public void TakeItNow(RenderTexture rt)
    {
        RenderTexture.active = rt;
        myCamera.targetTexture = rt;
        myCamera.Render();

        myCamera.targetTexture = null;
        RenderTexture.active = null;
    }

    private void TakeScreenShot(int width, int height)
    {
        myCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        takeSSOnNextFrame = true;
    }

    public static void TakeScreenShot_Static(int width, int height)
    {
        //instance.TakeScreenShot(width, height);
    }
}
