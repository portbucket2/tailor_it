﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragNDropHandler : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    //private RectTransform rectTransform;
    private Vector3 mouseOffset;
    private float zCord;
    private float xCord;
    public Camera mainCam;
    
    private void Awake()
    {
        //rectTransform = GetComponent<RectTransform>();
        mainCam = Camera.main;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        zCord = mainCam.WorldToScreenPoint(gameObject.transform.position).z;
        xCord = mainCam.WorldToScreenPoint(gameObject.transform.position).x;
        Debug.Log("Draging...");
        mouseOffset = gameObject.transform.position - GetMousePosition();
    }

    public void OnDrag(PointerEventData eventData)
    {
        
        Debug.Log("OnDrag");
        transform.position = GetMousePosition() + mouseOffset;
        //ModifySelectionObjectPosition();


    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Stopped draging");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown!");
    }

    public Vector3 GetMousePosition()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.y = 0;
        //mousePoint.x = xCord;
        mousePoint.z = zCord;
        return mainCam.ScreenToWorldPoint(mousePoint);

    }


    #region Vari Bhai

    private void ModifySelectionObjectPosition() {

        Vector3 t_MousePosition         = Input.mousePosition;
        Vector3 t_ScreenToWorldPoint = mainCam.ScreenToWorldPoint(t_MousePosition);
        Vector3 t_ModifiedPosition = new Vector3(
                t_ScreenToWorldPoint.x,
                transform.position.y,
                zCord
            );

        transform.position = t_ModifiedPosition + mouseOffset;


    }

    #endregion
}
