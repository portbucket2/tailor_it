﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateAndScale : MonoBehaviour
{

    public static RotateAndScale Instance;

    private float rotationSpeed = 20;
    private Vector3 lastMousePosition;
    public float scaleSensitivity = 2.0f;
    public RayCastHitPosition varObj;

    private Transform transformObj;
    

    public Slider rotate;
    
    

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        rotate.onValueChanged.AddListener(OnRotateSliderValueChanged);
        
        
        

    }
    public bool turnOn;
    
    
    private void OnRotateSliderValueChanged(float newValue)
    {
        Debug.Log("New slider value:" + newValue);
        turnOn  = newValue >= 1;
        Debug.Log("turnOn : " + turnOn);


    }
    

    
    
    public void OnMouseDrag()
    {
        if (turnOn)
        {
            
            if (transformObj != null)
            {

                float rotX = Input.GetAxis("Mouse X") * rotationSpeed * Mathf.Deg2Rad;
                transformObj.Rotate(Vector3.up, -rotX*100,Space.World);
                Debug.LogFormat("Mouse dragging.. {0}", transformObj.name);
                varObj.enabled = false;
                
            }
        }
        else
        {
            varObj.GetComponent<RayCastHitPosition>().enabled = true ;
        }

        
        
        
        
    }

    private void Update()
    {
        //ScaleUpOrDown(scaling);
    }

    
    

    public void SetSelectedObject(Transform transformObj)
    {
        this.transformObj = transformObj;   
    }

    public void DeelectObject() {

        transformObj = null;
    }

    

}
