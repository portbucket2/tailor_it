﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Animator))]
public class RayCastHitPosition : MonoBehaviour, IDragHandler
{
    #region Public Variables

    public static RayCastHitPosition Instance;
    public Canvas canvas;
    public Canvas rotAndSc;
    public bool dragging = false;
    
    
    public Animator anim;

    public Transform    defaultMovingObject;
    public Vector3      defaultOffsetForPosition;

    [Space(5.0f)]
    [Header("Configuretion  :   RayCasting")]
    public Camera       cameraReference;
    public LayerMask    clothLayerMask;

    #endregion

    #region Private Variables

    private Transform   m_MovingObject;
    private Vector3     m_OffsetForPosition;
    private float rotationSpeed = 20;
    private float scaling = 0.5f;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        //anim = GetComponent<Animator>();
    }

    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0)) {

            OnTouchDown(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {

            OnTouch(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {

            OnTouchUp(Input.mousePosition);
        }

#endif

#if UNITY_ANDROID || UNITY_IOS

        if (Input.touchCount > 0) {

            switch (Input.GetTouch(0).phase) {

                case TouchPhase.Began:
                    OnTouchDown(Input.mousePosition);
                    break;
                case TouchPhase.Stationary:

                    break;
                case TouchPhase.Moved:
                    OnTouch(Input.mousePosition);
                    break;
                case TouchPhase.Ended:
                    OnTouchUp(Input.mousePosition);
                    break;
                case TouchPhase.Canceled:
                    OnTouchUp(Input.mousePosition);
                    break;
            }
        }

#endif
    }

    #endregion

    #region Configuretion

    private void OnTouchDown(Vector3 t_TouchPosition) {

        RayCastOnWorldSpace(t_TouchPosition);
    }

    private void OnTouch(Vector3 t_TouchPosition)
    {
        RayCastOnWorldSpace(t_TouchPosition);
    }

    private void OnTouchUp(Vector3 t_TouchPosition)
    {
        RayCastOnWorldSpace(t_TouchPosition);
    }

    private void RayCastOnWorldSpace(Vector3 t_TouchPosition) {
        if(dragging)
        {
            Ray t_Ray = cameraReference.ScreenPointToRay(t_TouchPosition);
            RaycastHit t_RayCastHit;

            if (Physics.Raycast(t_Ray, out t_RayCastHit, 1000f, clothLayerMask))
            {

                if (t_RayCastHit.collider != null)
                {

                    m_MovingObject.position = t_RayCastHit.point + m_OffsetForPosition;
                    //Debug.Log(m_MovingObject.position);
                }
            }

            dragging = false;
        }
        else
        {
            
        }
        
    }

    #endregion

    #region Public Callback

    public void MoveThisObject(Transform t_MovingObject = null, Vector3 t_OffsetForPosition = new Vector3()) {

        if (t_MovingObject == null)
            m_MovingObject = defaultMovingObject;
        else
            m_MovingObject = t_MovingObject;

        if (t_OffsetForPosition == Vector3.zero)
            m_OffsetForPosition = defaultOffsetForPosition;
        else
            m_OffsetForPosition = t_OffsetForPosition;
    }

    

    public void CameraTransition()
    {
        Debug.Log("Camera Transition Called");
        anim.SetBool("Transition", true);
        //canvas.enabled = false;
        rotAndSc.enabled = true;
        
    }

    public void Rotate()
    {

    }

    public void OnDrag(PointerEventData eventData)
    {
        dragging = true;
        Debug.Log("Dragging " + dragging);
    }


    #endregion
}
