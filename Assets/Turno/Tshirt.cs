﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tshirt : MonoBehaviour
{
    public static Tshirt instance;
    public static Tshirt Instance { get { return instance; } }

    public RenderTexture rt;

    public Texture2D texture2D;
    public SnapShotScript snap;

    private void Start()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            ApplyTexture();
        }
    }


    public void ApplyTexture()
    {
        snap.TakeItNow(rt);
        gameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", rt);

    }
}
