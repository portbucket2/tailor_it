﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mask : MonoBehaviour
{
    [SerializeField]

    private Transform prefab;
    private Transform[] transformInSequence = new Transform[3];
    public Sprite img;
    int i = 0;
    

   
    
    //Quaternion rotation = Quaternion.Euler(0, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        Debug.Log("On Click Called");
        Transform t_TransformReferencve =  ((GameObject) Instantiate(prefab.gameObject, transform.position, prefab.transform.rotation)).transform;
        while(i < transformInSequence.Length)
        {
            transformInSequence[i] = t_TransformReferencve;
            i++;
        }
        
        RayCastHitPosition.Instance.MoveThisObject(t_TransformReferencve);
        RayCastHitPosition.Instance.CameraTransition();
        RotateAndScale.Instance.SetSelectedObject(t_TransformReferencve);
        Scale.instance.SetSelectedObject(t_TransformReferencve);
        Scale.instance.scale.value = 0.8f;
        RotateAndScale.Instance.turnOn = false;
        //t_TransformReferencve.GetComponent<RayCastHitPosition>().enabled = true;
        
    }

    
}
