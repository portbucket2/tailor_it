﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    [SerializeField]
    public Material[] materials;
    public Renderer rend;
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = materials[0];
    }





    private void Update()
    {
        
        
        if(gameObject.transform.position.y < -12)
        {
            rend.sharedMaterial = materials[1];
        }
    }

    public void DipIt()
    {
        StartCoroutine(DipCloth());
    }

    

    IEnumerator DipCloth()
    {
        gameObject.transform.Translate(0.0f, -18.0f, 0f);
        yield return new WaitForSeconds(5);
        gameObject.transform.Translate(0.0f, 18.0f, 0f);
    }










}
